import IP from 'ip'
import {ipcMain,dialog,app} from "electron"
import Jimp from 'jimp';
import au from "../utils/auHelper"
import imageHelper from "./imageHelper"
const fs = require('fs');


//获取内网ip
ipcMain.handle('getIPAddress', (event, someArgument) => {
    return IP.address();
})

//裁剪
ipcMain.handle('image-cart', (event, someArgument) => {
    let image =someArgument.image;
    let info=someArgument.info;
    return new Promise((resolve,rej)=>{
        Jimp.read(image).then(img=>{
            let path=imageHelper.createdFilePath(someArgument.id);
            img.crop( info.left, info.top, info.width, info.height).quality(100)
            .writeAsync(path);
            resolve(path);
        })
    });
})
//获取坐标像素点
ipcMain.handle('image-point-color', (event, someArgument) => {
    let image =someArgument.image;
    let info=someArgument.info;
    return new Promise((resolve,rej)=>{
        Jimp.read(image).then(img=>{
            console.log(img.getPixelColor(info.x, info.y))
            resolve(img.getPixelColor(info.x, info.y));
        })
    });
})

//删除临时文件
ipcMain.handle('image-del', (event, id) => {
    imageHelper.FileDelPathAll(imageHelper.getImageFilePath(id))
})

//保存文件到本地
ipcMain.handle('image-save', (event, src) => {
    //选择文件夹
    let fileSavePath=dialog.showSaveDialogSync(null,{
        title:"保存图片",
        buttonLabel:"保存",
        defaultPath:"1.png",
        filters: [
            { name: '图片', extensions: ['jpg', 'png', 'gif'] },
        ]
    });
    console.log(src,fileSavePath)
    if(fileSavePath){
        fs.createReadStream(src).pipe(fs.createWriteStream(fileSavePath));
        //fs.writeFileSync(fileSavePath, fs.readFileSync(src));
        return true;
    }
})
