import { createStore } from 'vuex'
import Vue from "vue"

export default createStore({
  state: {
    ipAddress:null,
    dataList:[],
    selectedIndex:0,
    //客户端列表
    clientList:[],
    clientSelectKey:null
  },
  mutations: {
    addData(state,obj){
      state.dataList.push(obj);
      state.selectedIndex=state.dataList.length-1;
    },
    removeData(state,id){
      let list=[];
      for (let index = 0; index < state.dataList.length; index++) {
        if(state.dataList[index].id!=id){
          list.push(state.dataList[index])
        }
      }
      // Vue.$set(state, 'dataList', list)
      state.dataList=list
      //state.dataList=list
    },
    setSelectIndex(state,index){
      state.selectedIndex=index||0;
    },
    //客户端操作
    setClient(state,obj){
      state.clientList=obj;
    },
    removeClient(state,index){
      state.clientList.splice(index,1);
    },
    setClientSelectKey(state,index){
      state.clientSelectKey=index||null;
    },
    //设置ip地址
    setIpAddress(state,ip){
      state.ipAddress=ip;
    },
  },
  getters:{
    thenData(state){
      return state.dataList[state.selectedIndex];
    },
    thenClient(state){
      return state.clientList.filter(x=>x.ip==state.clientSelectKey)[0];
    }
  },
  actions: {

  },
  modules: {
  }
})
