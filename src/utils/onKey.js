class onKey{
    on(key,callback){
        let _this=this;
        let isEnt=false;
        for(let i=0;i<_this.onKeys.length;i++){
            if(_this.onKeys[i].key==key){
                _this.onKeys[i].callback=callback;
                isEnt=true;
            }
        }
        if(!isEnt){
            _this.onKeys.push({
                key:key,
                callback:callback
            });
        }
    }
    un(key){
        let _this=this;
        for(let i=0;i<_this.onKeys.length;i++){
            if(_this.onKeys[i].key==key){
                _this.onKeys.splice(i,1)
            }
        }
    }
    constructor(isInput=true,isTest=false){
        let keyCode={
            17:'ctrl',
            16:'shift',
            46:'del',
            35:'end',
            36:'home',
            45:'ins',
            33:'pageUp',
            34:'pageDown',
            107:'+',
            109:'-',
            32:'Space',
            13:'Enter',
        };
        let _this=this;
        _this.keyCodes=keyCode;
        _this.onKeys=[];
        _this.isTest=isTest;
        _this.isInput=isInput;
        document.addEventListener('keydown',function(e){
            let e_key=keyCode[e.keyCode]||e.key;
            if(!e_key){
                return;
            }
            e_key=e_key.toLowerCase();
            e.e_key=e_key;
            if(keyCode[e.keyCode]){
                _this[keyCode[e.keyCode]]=true;
            }else{
                _this[e_key]=true;
            }
            if(_this.isTest){
                console.log({
                    keyName:e_key,
                    keyDown:'true',
                    onKey:_this,
                    event:e
                });
            }
            let isStop=false;
            //监听
            if(_this.onKeys && _this.onKeys instanceof Array){
                //解析
                for(let i=0;i<_this.onKeys.length;i++){
                    let it=_this.onKeys[i];
                    let keys=it.key;
                    if(keys instanceof Array){
                        if(keys.includes(e_key)){
                            let isStop2=true;
                            keys.filter(x=>x.toLowerCase()!=e_key).forEach((key,i)=>{
                                if(!_this[key]){
                                    isStop2=false;
                                }
                            });
                            if(isStop2){
                                //触发事件
                                it.callback && it.callback.call(_this,e,keys);
                            }
                            (!isStop) && (isStop=isStop2);
                        }
                    }else if(typeof keys==='string'){
                        let isStop2=e_key==keys.toLowerCase();
                        if(isStop2){
                            //触发事件
                            it.callback && it.callback.call(_this,e,keys);
                        }
                        (!isStop) && (isStop=isStop2);
                    }
                }
            }
            //组织冒泡
            if(_this.isTest||(isStop && !(!_this.isInput && e.target.selectionDirection && e.target.selectionDirection=="forward"))){
                e.returnValue=false;
                return false;
            }
        });
        document.addEventListener('keyup',function(e){
            //阻止
            let e_key=keyCode[e.keyCode]||e.key;
            if(!e_key){
                return;
            }
            e_key=e_key.toLowerCase();
            if(keyCode[e.keyCode]){
                _this[keyCode[e.keyCode]]=false;
            }else{
                _this[e_key]=false;
            }
            if(_this.isTest){
                console.log({
                    keyName:e_key,
                    keyDown:'true',
                    onKey:_this,
                    event:e
                });
            }
        });
    }
}
export default onKey
