/**
 * 监听客户端
 */
import {ipcRenderer} from "electron"
import Vue from "vue"

export default function (app) {
    //初始化获取客户端列表
    ipcRenderer.invoke("wss-init").then(info => {
        app.$store.commit('setIpAddress', info.ip);
        app.$store.commit('setClient', info.clients);
        app.$notify({
            message: '初始化完成'
        });
    });
    let timer=setInterval(() => {
        ipcRenderer.invoke("wss-client").then(info => {
            if(info && app.$store.state.clientSelectKey && info.filter(x=>x.ip==app.$store.state.clientSelectKey).length<1){
                app.$store.commit('setClientSelectKey', null);
            }
            if(!app.$store.state.clientSelectKey && info && info.length>0){
                app.$store.commit('setClientSelectKey', info[0].ip);
            }
            app.$store.commit('setClient', info);
        });
    }, 1000);
}
