import base64 from "base"
export default {
    /**
     * 根据map计算滚动条位置
     * @param e mousedown事件
     * @param outerDiv 外层div
     * @param scrollSize 滚动条的宽高width、height
     * @param callback 回调函数
     * @constructor
     */
    MouseSliding(e, odiv, outerDiv, scrollSize, callback, closeBack) {
        odiv = odiv || e.target;
        let oe = e;
        //算出鼠标相对元素的位置
        let disX = e.clientX - odiv.offsetLeft;
        let disY = e.clientY - odiv.offsetTop;
        //获取到外层元素宽高
        let imageSizeWidth = outerDiv.offsetWidth;
        let imageSizeHeight = outerDiv.offsetHeight;
        let hover = (e) => {
            //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
            let left = e.clientX - disX;
            let top = e.clientY - disY;

            let maxLeft = imageSizeWidth - odiv.offsetWidth;
            let maxTop = imageSizeHeight - odiv.offsetHeight;

            if (left < 0) {
                left = 0;
            } else if (left > maxLeft) {
                left = maxLeft
            }
            if (top < 0) {
                top = 0;
            } else if (top > maxTop) {
                top = maxTop
            }

            //设置滚动条位置
            if (scrollSize) {
                let scrollTop = top / imageSizeHeight * scrollSize.height;
                let scrollLeft = left / imageSizeWidth * scrollSize.width;
                callback({ left: left, top: top, scrollTop, scrollLeft })
            } else {
                callback({ left: left, top: top, width: e.clientX - oe.clientX, height: e.clientY - oe.clientY })
            }

        }
        let close = (e) => {
            window.removeEventListener("mousemove", hover, false);
            window.removeEventListener("mouseup", close, false);
            closeBack && closeBack()
        }
        window.addEventListener("mousemove", hover);
        window.addEventListener("mouseup", close);
    },
    /**
     * 根据滚动条计算map位置
     * @param scrollSize 滚动条大小
     * @param pageSize 容器大小和坐标
     * @param imageDiv map容器元素
     * @returns {{top: number, left: number, width: number, height: number}}
     * @constructor
     */
    MapSliding(scrollSize, pageSize, imageDiv) {
        let size = scrollSize;
        if (size.width == 0 || size.height == 0) {
            return { width: 0, height: 0, top: 0, left: 0 };
        }
        let pageSizeWidth = pageSize.width;
        let pageSizeHeight = pageSize.height;
        if (pageSizeWidth > size.width) {
            pageSizeWidth = size.width;
        }
        if (pageSizeHeight > size.height) {
            pageSizeHeight = size.height;
        }
        let imageSizeWidth = imageDiv.offsetWidth;
        let imageSizeHeight = imageDiv.offsetHeight;

        let width = imageSizeWidth * (pageSizeWidth / size.width);
        let height = imageSizeHeight * (pageSizeHeight / size.height);
        let top = imageSizeWidth / size.width * pageSize.top;
        let left = imageSizeHeight / size.height * pageSize.left;
        return { width, height, top, left };
    },
    imageBase64UrlToBlob(base64) {
        var type = base64.split(",")[0].match(/:(.*?);/)[1];//提取base64头的type如 'image/png'    
        var bytes = window.atob(base64.split(',')[1]);//去掉url的头，并转换为byte (atob:编码 btoa:解码)
        //处理异常,将ascii码小于0的转换为大于0
        //var ab = new ArrayBuffer(bytes.length);//通用的、固定长度(bytes.length)的原始二进制数据缓冲区对象
        var ia = new Uint8Array(bytes.length);
        for (var i = 0; i < bytes.length; i++) {
            ia[i] = bytes.charCodeAt(i);
        }
        let blob = new Blob([ia], { type: type });
        console.log(blob)
    },
    imageBlobToBase64(blob, callback) {
        let a = new FileReader();
        a.onload = function (e) { callback(e.target.result); }
        a.readAsDataURL(blob);
    },
    imageBase64toFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, { type: mime });
    },
    imageBase64ToUint8Array(base64) {
        var bytes = window.atob(base64.split(',')[1]);
        var ia = new Uint8Array(bytes.length);
        for (var i = 0; i < bytes.length; i++) {
            ia[i] = bytes.charCodeAt(i);
        }
        return ia;
    },
    iamgeUint8arrayToBase64(u8Arr) {
        let CHUNK_SIZE = 0x8000; //arbitrary number
        let index = 0;
        let length = u8Arr.length;
        let result = '';
        let slice;
        while (index < length) {
            slice = u8Arr.subarray(index, Math.min(index + CHUNK_SIZE, length));
            result += String.fromCharCode.apply(null, slice);
            index += CHUNK_SIZE;
        }
        // web image base64图片格式: "data:image/png;base64," + b64encoded;
        return  "data:image/png;base64," + (this.window?btoa(result):result.toString("base64"));
    }
}
