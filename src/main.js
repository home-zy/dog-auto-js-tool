import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import auClient from  './utils/auClient'
import onKey from "@/utils/onKey";
import auHelper from "@/utils/auHelper";


let _app=createApp(App);

const key=new onKey(false);

_app.config.globalProperties.$key=key;
_app.config.globalProperties.$au=auHelper;

const vmApp=_app.use(store)
    .use(router)
    .use(ElementPlus)
    .mount('#app');
console.log(vmApp);

//获取客户端列表
auClient(vmApp);



