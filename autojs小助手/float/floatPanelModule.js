
module.exports=function(_self){
    var floatPanel=require("./floatPanel.js")(_self);

    _self.auInfo={
        headerHeight:30,
        headerTextSize:"12sp",
        border:"#ffffff",
        //默认样式
        headerBg:"#000000",
        headerColor:"#ffffff",
        //选中样式
        selectHeaderColor:"#000000",
        selectHeaderBg:"#ffffff",
    };


    floatPanel.setContent(
        <vertical>
            <HorizontalScrollView>
            <linear orientation="horizontal" scrollbar="true" id="auHeader" h="{{auInfo.headerHeight}}" padding="0" bg="{{auInfo.border}}">

            </linear>
            </HorizontalScrollView>
            <text bg="{{auInfo.border}}" w="*" h="1px"></text>
            <ScrollView>
            <linear orientation="vertical" id="auContent" layout_weight="1" padding="2">
            </linear>
            </ScrollView>
        </vertical>
    );

    var selectKey="";
    var menus={};

    //刷新ui
    function updaterUi(){
        ui.run(()=>{
            Object.keys(menus).forEach(function(key){
                let menuItem=menus[key];
                let dom=floatPanel.win["auHeader"+key];
                if(dom){
                    //刷新
                    if(selectKey==key){
                        menuItem.head.attr("bg",auInfo.selectHeaderBg);
                        menuItem.head.setTextColor(colors.parseColor(auInfo.selectHeaderColor));
                        menuItem.body.setVisibility(0);
                        menuItem.event.emit("show",menuItem._evnetObj);
                    }else{
                        menuItem.head.attr("bg",auInfo.headerBg);
                        menuItem.head.setTextColor(colors.parseColor(auInfo.headerColor));
                        menuItem.body.setVisibility(8);
                    }
                }
            })
    
        })
    }

    function removeNode(node){
        ui.run(()=>{
            var parent = node.getParent();
            if (parent != null) {
                parent.removeView(node);
            }
        })
    }


    floatPanel.setWidth(260);
    floatPanel.setHeight(360);

    let obj={
        addMenu(key,title,view,callback){
            if(menus[key]){
                throw new Error("key已存在");
            }

            var auEvent=events.emitter();

            let auNode=(function(headerView){
                return ui.run(()=>{
                    let head=ui.inflate(headerView,floatPanel.win.auHeader,true);
                    let body=ui.inflate(view,floatPanel.win.auContent,true)

                    head.attr("id","auHeader"+key);
                    head.text(title);

                    head.on("click",function(){
                        menus[selectKey] && menus[selectKey].event.emit("hide",menus[selectKey]._evnetObj);
                        selectKey=key;
                        updaterUi();
                    })

                    return {
                        title:title,
                        key:key,
                        head:head,
                        body:body
                    }
                });
            })(<text textSize="{{auInfo.headerTextSize}}" w="auto" padding="10 0" h="*" marginRight="1px" bg="{{auInfo.headerBg}}" textColor="{{auInfo.headerColor}}" gravity="center" layout_gravity="center">标题</text>)

            //广播上一个隐藏
            menus[selectKey] && menus[selectKey].event.emit("hide",menus[selectKey]._evnetObj);

            if(Object.keys(menus).length<1){
                selectKey=key;
            }

            menus[key]={
                title:title,
                body:auNode.body,
                head:auNode.head,
                event:auEvent,
                _evnetObj:auNode
            };

            
            updaterUi();

            if(callback){
                callback({
                    node:auNode,
                    event:auEvent
                },floatPanel);
            }
            return auNode;
        },
        removeMenu(key){
            if(menus[key]){
                removeNode(menus[key].head);
                removeNode(menus[key].body);
                menus[key].event.emit("close",{
                    title:menus[key].title,
                    key:menus[key].key,
                });
                delete menus[key];
                if(selectKey==key){
                    selectKey=Object.keys(menus)[0]||""
                }
                updaterUi();
            }
        },
        showMenu(key){
            menus[selectKey] && menus[selectKey].event.emit("hide",menus[selectKey]._evnetObj);

        }
    };
    Object.assign(obj,floatPanel);
    return obj;
};
