
module.exports=function(_self){
    var winInfo={
        width:180,
        height:240,
        headerHeight:30,
        btnWidth:20,
        openBtnWidth:40,
        isEdge:false
    };
    _self.winInfo=winInfo;
    var global={};

    //创建广播
    var auEvent=events.emitter();

    
    var w = floaty.rawWindow(
        <vertical id="app">
            <img id="btnOpen" circle="true" w="{{winInfo.openBtnWidth}}" h="{{winInfo.openBtnWidth}}" alpha=".5" src="https://soxml.com/autojs/all/open1.png?v=1"/>
            <horizontal radius="20" id="header" bg="#000000" paddingLeft="4"  paddingRight="4" w="{{winInfo.width}}" h="{{winInfo.headerHeight}}" alpha=".8">
                <text id="headerTitle" layout_weight="1" layout_gravity="center" textColor="#ffffff" textSize="14sp">易优工具箱</text>
                <img id="btnClose" padding="2 0" layout_gravity="center|right" w="{{winInfo.btnWidth}}" h="{{winInfo.btnWidth}}" src="https://soxml.com/autojs/all/close.png"/>
                <img id="btnMobile" padding="2 0"  layout_gravity="center|right" w="{{winInfo.btnWidth}}" h="{{winInfo.btnWidth}}" src="https://soxml.com/autojs/all/mobile.png"/>
            </horizontal>
            <vertical id="content" bg="#000000" w="{{winInfo.width}}" h="{{winInfo.height-winInfo.headerHeight}}" alpha=".64"></vertical>
        </vertical>
    );
    
    
    w.setTouchable(true);
    w.setPosition(0,device.height/3);
    //设置面板位置
    function setScreenEdge(){
        function setEdg(aw,ah){
            let minX=w.getX();
            let minY=w.getY();
            
            ah=ah||w.getHeight();
            aw=aw||w.getWidth();
    
            winInfo.realWidth=aw;
            winInfo.realHeight=ah;
    
            let obj={x:minX,y:minY};
            if(minX+aw>device.width){
                obj.x=device.width-aw;
            }else if(minX<0){
                obj.x=0;
            }
    
            if(minY>device.height-ah){
                obj.y=device.height-ah;
            }else if(minY<0){
                obj.y=0;
            }
            w.setPosition(obj.x,obj.y);
        }
        if(winInfo.realWidth && winInfo.realHeight){
            setEdg(winInfo.realWidth,winInfo.realHeight);
        }else{
            setTimeout(()=>{
                setEdg(w.getWidth(),w.getHeight());
            },100)
        }
    }
    //设置按钮位置
    function setOpenEdge(){
        if(!winInfo.realWidth){
            return ;
        }
        function setEdg(wid){
            wid=wid||w.getWidth();
            winInfo.realOpenWidth=wid;
            let x=w.getX()+winInfo.realWidth/2;
            if(x>device.width/2){
                //右边
                w.setPosition(device.width-wid,w.getY())
            }else{
                //左边
                w.setPosition(0,w.getY());
            }
        }
        if(winInfo.realOpenWidth){
            setEdg(winInfo.realOpenWidth);
        }else{
            setTimeout(()=>{
                setEdg(w.getWidth());
            },100)
        }
    }
    //拖动
    function win_toouchMove(win,open,isGrill){
        let position;
        open.on('touch_down', (e) => {
            if (!position) {
                //获取x和win.x的差，后面在点击减去差
                position = [e.getRawX()-win.getX(), e.getRawY()-win.getY()];
                global.isNoHide=false;
            }
        });
        open.on('touch_up', (e) => {
            if(position && isGrill){
                let x=e.getRawX()-position[0];
                let y=e.getRawY()-position[1];
                let wid=w.getWidth();
                if(x+wid/2>device.width/2){
                    //右边
                    w.setPosition(device.width-w.getWidth(),y)
                }else{
                    //左边
                    w.setPosition(0,y);
                }
            }
            position = null;
        });
        open.on('touch_move', (e) => {
            if (position) {
                global.isNoHide=true;
                let [x, y] = position;
                win.setPosition(e.getRawX()-x, e.getRawY()-y);
            }
        });
        open.on('click',()=>{});
    }
    
    function winHide(){
        ui.run(()=>{
            if(global.isNoHide){
                return false;
            }
            //关闭
            w.btnOpen.attr('w',40);
            w.btnOpen.attr('h',40);
            w.header.attr('w',0);
            w.header.attr('h',0);
            w.content.attr('w',0);
            w.content.attr('h',0);
            //设置按钮位置
            if(winInfo.isEdge){
                setOpenEdge();
            }else if(global.btnPoint && global.btnPoint.x>device.width-winInfo.realWidth){
                //还原位置
                w.setPosition(global.btnPoint.x,w.getY());
            }
            auEvent.emit("hide")
        })
    }
    function winShow(){
        ui.run(()=>{
            //记录位置
            global.btnPoint={
                x:w.getX(),
                y:w.getY(),
            };
            //开启
            
            w.btnOpen.attr('w',0);
            w.btnOpen.attr('h',0);
            w.header.attr('w',winInfo.width);
            w.header.attr('h',winInfo.headerHeight);
            w.content.attr('w',winInfo.width);
            w.content.attr('h',winInfo.height);
        
            //判断是否超出，超出则设置其位置
            setScreenEdge();
            auEvent.emit("show")
        })
    }
    function winClose(){
        toast('退出成功');
        auEvent.emit("close")
        exit();
    }
    //初始化
    function win_init(){
        //滑动监听
        win_toouchMove(w,w.header,false);
        win_toouchMove(w,w.btnMobile,false);
        win_toouchMove(w,w.btnOpen,false);
        
        w.btnClose.on('click',winClose);
        w.btnMobile.on('click',winHide);
        w.header.on('click',winHide);
        w.btnOpen.on('click',winShow);
    }
    
    w.btnOpen.attr('w',40);
    w.btnOpen.attr('h',40);
    w.header.attr('w',0);
    w.header.attr('h',0);
    w.content.attr('w',0);
    w.content.attr('h',0);
    
    ui.run(win_init);
    
    
    setInterval(()=>{}, 1000);
    
    return {
        win:w,
        show:winShow,
        hide:winHide,
        close:winClose,
        setWidth(val){
            winInfo.width=val;
        },
        setHeight(val){
            winInfo.height=val;
        },
        setContent:function(content){
            return ui.run(()=>ui.inflate(content,w.content,true));
        },
        setTitle:function(title){
            return ui.run(()=>w.headerTitle.text(title));
        },
        event:auEvent
    };
};